/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package alxio;

import static alxio.BFSNode.random;
import core.game.Observation;
import core.game.StateObservation;
import java.util.ArrayDeque;
import java.util.ArrayList;
import ontology.Types;

/**
 *
 * @author ALX
 */
public class Preserver {

    static int NEEDED_DEPTH = 10;

    int neededDepth = 0;
    int childId = 0;
    int totalSteps = 0;

    double[] values = new double[Game.NUM_ACTIONS];
    int[] count = new int[Game.NUM_ACTIONS];
    boolean[] lost = new boolean[Game.NUM_ACTIONS];
    
    StateObservation[][] observations = new StateObservation[Game.NUM_ACTIONS][2];

    public Preserver(StateObservation rootState) {
        neededDepth = Math.max(Strategus.DETERMINISTIC_CHOICE ? 0 : 2, NEEDED_DEPTH);

        for (int i = 0; i < Game.NUM_ACTIONS; ++i) {
            StateObservation advanced = rootState.copy();
            advanced.advance(Game.actions[i]);
            if(advanced.getGameWinner() == Types.WINNER.PLAYER_LOSES){
                lost[i] = true;
                continue;
            }
            observations[i][0] = advanced;
            observations[i][1] = advanced.copy();
            observations[i][0].advance(Types.ACTIONS.ACTION_NIL);
            observations[i][1].advance(Game.actions[i]);
        }
    }

    public boolean step() {
        return false;
//        StateObservation obs = falseRoot.stateObs.copy();
//        obs.advance(Game.actions[childId % Game.NUM_ACTIONS]);
//        for (int i = 0; i < C.ROLLOUT_LENGTH && !obs.isGameOver(); ++i) {
//            //vis[childId][i]++;
//            double val = obs.getGameScore();
//            obs.advance(Game.actions[random.nextInt(Game.actions.length)]);
//            double delta = obs.getGameScore() - val;
//            if (obs.isGameOver() && obs.getGameWinner() == Types.WINNER.PLAYER_LOSES) {
//                delta -= 10000;
//            }
//            values[childId % Game.NUM_ACTIONS] += delta / (i + 1);
//            Strategus.onAdvance(obs);
//            //if(delta < 0) lose[childId][i] += delta;
//        }
//        ++count[childId % Game.NUM_ACTIONS];
//        if (++childId == 2 * Game.NUM_ACTIONS) {
//            childId = 0;
//            ++totalSteps;
//        }
//        return totalSteps < 1 || totalSteps < 10 * ChildNodes.otherCount / (ChildNodes.sameCount + 1);
    }

    public boolean anythingMoved(StateObservation s1, StateObservation s2) {
        if (!s1.getAvatarPosition().equals(s2.getAvatarPosition())) {
            return true;
        }
        ArrayList<Observation>[][] grid1 = s1.getObservationGrid();
        ArrayList<Observation>[][] grid2 = s2.getObservationGrid();
        if (grid1.length != grid2.length) {
            return true;
        }
        if (grid1.length == 0) {
            return false;
        }
        if (grid1[0].length != grid2[0].length) {
            return true;
        }
        if (grid1[0].length == 0) {
            return false;
        }
        for (int i = 0; i < grid1.length; ++i) {
            for (int j = 0; j < grid1.length; ++j) {
                ArrayList<Observation> o1 = grid1[i][j];
                ArrayList<Observation> o2 = grid2[i][j];
                if (o1 == null) {
                    if (o2 == null) {
                        continue;
                    }
                    return true;
                }
                if (o1.size() != o2.size()) {
                    return true;
                }
                for (int k = 0; k < o1.size(); ++k) {
                    Observation a1 = o1.get(k);
                    Observation a2 = o2.get(k);
                    if (a1.itype != a2.itype) {
                        return true;
                    }
                    if (!a1.position.equals(a2.position)) {
                        return true;
                    }
                }
            }
        }
        return false;
    }
}
