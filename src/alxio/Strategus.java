/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package alxio;

import core.game.StateObservation;
import ontology.Types;

/**
 *
 * @author ALX
 */
public class Strategus {
    private static int lastTreeSize = 0;
    
    public static double ROLLOUT_CHANCE = 0;
    public static double SIGTH_RADIUS = 1;
    public static double SAME_COUNT_TO_BE_SURE = 2;
    public static double TOTAL_COUNT_TO_BE_SURE = 10;
    
    public static boolean DETERMINISTIC_CHOICE = true;
    
    public static int MAX_TREE_SIZE = 5000;
    
    private static int sight = 109;
    
    
    private static double lastShrink = 0;
    
    private static int lastSame = 0;
    private static int lastOther = 0;
    public static boolean MCTSChoice = false;
    
    public static double TOTAL_MOVES = 0;
    public static double DEAD_MOVES = 0;
    static boolean UPDATE_NODES = false;
    
    public static double getSimpleRolloutChance(){
        return 0.5 * Math.sqrt(DEAD_MOVES / (TOTAL_MOVES + 1));
    }
    
    public static void onAdvance(StateObservation newState){
        if(newState.isGameOver() && newState.getGameWinner() == Types.WINNER.PLAYER_LOSES){
            //Debug.log(3,"DEAD!\nDEAD\nDEAD\nDEAD\n");
            ++DEAD_MOVES;
        }
        ++TOTAL_MOVES;
    }
    
    public static void updateStrategy(int treeSize){
        Debug.Log(1, "Death rate: " + Math.sqrt(DEAD_MOVES / (TOTAL_MOVES + 1)));
        TOTAL_MOVES = 0;
        DEAD_MOVES = 0;
        Runtime runtime = Runtime.getRuntime();
        long used = runtime.totalMemory() - runtime.freeMemory();
        long max = (long) (0.75 * runtime.maxMemory());
        
        if (used > max && used > lastShrink) {
            MAX_TREE_SIZE = (int) (0.75 * MAX_TREE_SIZE);
            lastShrink = used * 1.1;
        }
        
        ROLLOUT_CHANCE = Math.pow(treeSize / MAX_TREE_SIZE, 8);
        
        lastSame = ChildNodes.sameCount;
        lastOther = ChildNodes.otherCount;
        
        UPDATE_NODES = ChildNodes.sameCount < ChildNodes.otherCount;
        
        if(ChildNodes.otherCount == 0 && ChildNodes.sameCount > 200){
            SAME_COUNT_TO_BE_SURE = 1;
            TOTAL_COUNT_TO_BE_SURE = 2;
            DETERMINISTIC_CHOICE = true;
        }else{
            SAME_COUNT_TO_BE_SURE = (10 + treeSize) * (ChildNodes.otherCount + 5) / (ChildNodes.sameCount + 5);
            TOTAL_COUNT_TO_BE_SURE = 10 * SAME_COUNT_TO_BE_SURE;
            DETERMINISTIC_CHOICE = false;
        }
    }

    static boolean needSimple() {
        UPDATE_NODES = true;
        return true;
        //TODO also set UPDATE_NODES
    }
}
