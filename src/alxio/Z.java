package alxio;

import core.game.Observation;
import core.game.StateObservation;
import java.awt.Dimension;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;
import java.util.TreeSet;
import ontology.Types;
import tools.Vector2d;

/**
 *
 * @author ALX
 */
public class Z {

    public static long hash(StateObservation so) {
        return instance.internalHash(so);
    }

    public static void init(int x, int y) {
        instance = new Z(new MersenneTwisterFast(), x, y);
    }

    private Z(MersenneTwisterFast r, int x, int y) {
        this.x = x;
        this.y = y;
        this.random = r;
        orientationsHashes = new long[orientations.length];
        for (int i = 0; i < orientationsHashes.length; ++i) {
            orientationsHashes[i] = random.nextLong();
        }
    }

    private static Z instance = null;

    private Vector2d[] orientations = {Types.UP, Types.LEFT, Types.RIGHT, Types.DOWN, Types.NIL, Types.NONE};
    private long[] orientationsHashes;
    
    private long internalHash(StateObservation so) {
        long hash = Double.doubleToLongBits(so.getGameScore());
        Vector2d ori = so.getAvatarOrientation();
        for(int i=0;i<orientations.length;++i){
            if(orientations[i].equals(ori)){
                hash^=orientationsHashes[i];
                break;
            }
        }
        //so.getFromAvatarSpritesPositions();

        ArrayList<Observation>[][] grid = so.getObservationGrid();
        if (grid.length != y || grid[0].length != x) {
            throw new RuntimeException("Bad array size");
        }

        //double radius = (2 + Strategus.SIGTH_RADIUS * Math.max(y, x)) * so.getBlockSize();
        for (int i = 0; i < y; ++i) {
            for (int j = 0; j < x; ++j) {
                ArrayList<Observation> obs = grid[i][j];
                Set<Integer>used = new TreeSet<>();
                for (Observation o : obs) {
                    initType(o.itype);
                    if(!used.contains(o.itype)){
                        used.add(o.itype);
                        //if (o.category != TYPE_NPC) {
                            hash ^= values[o.itype][i][j];
                        //}
                    }
                }
            }
        }

        /*ArrayList<Observation>[] npcs = so.getNPCPositions();
        if (npcs != null && npcs.length > 0) {
            Vector2d player = so.getAvatarPosition();
            Dimension worldSize = so.getWorldDimension();
            double scalex = 0.5 * x / (Math.sqrt(player.x) + Math.sqrt(worldSize.width - player.x));
            double scaley = 0.5 * y / (Math.sqrt(player.y) + Math.sqrt(worldSize.height - player.y));
            double midx = Math.sqrt(player.x) * scalex;
            double midy = Math.sqrt(player.y) * scaley;
            
            for (ArrayList<Observation> list : npcs) {
                Set<IntPair>used = new HashSet<>();
                for (Observation o : list) {
                    initType(o.itype);
                    
                    Vector2d npc = o.position.copy().subtract(player);
                    npc.x = Math.signum(npc.x) * Math.sqrt(Math.abs(npc.x)) * scalex + midx;
                    npc.y = Math.signum(npc.y) * Math.sqrt(Math.abs(npc.y)) * scaley + midy;
                    IntPair pos = new IntPair((int)npc.x, (int)npc.y);
                    if(used.contains(pos)) continue;
                    used.add(pos);
                    hash ^= values[o.itype][pos.y][pos.x];
                }
            }
        }*/

        //for(n so.)
        //Debug.log(6, "NPC: " + count);
        return hash;
    }

    private void initType(int id) {
        if (values[id] == null) {
            values[id] = new long[y][x];
            for (int i = 0; i < y; ++i) {
                for (int j = 0; j < x; ++j) {
                    values[id][i][j] = random.nextLong();
                }
            }
        }
    }

    int y;
    int x;
    MersenneTwisterFast random;
    long[][][] values = new long[1001][][];

    public static final int TYPE_AVATAR = 0;
    public static final int TYPE_RESOURCE = 1;
    public static final int TYPE_PORTAL = 2;
    public static final int TYPE_NPC = 3;
    public static final int TYPE_STATIC = 4;
    public static final int TYPE_FROMAVATAR = 5;
    public static final int TYPE_MOVABLE = 6;
}
