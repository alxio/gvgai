/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package alxio;

import core.game.Observation;
import core.game.StateObservation;
import java.util.ArrayList;
import tools.Vector2d;

/**
 *
 * @author ALX
 */
public class Punisher {

    Punisher(ArrayList<Observation>[][] grid) {
        ySize = grid[0].length;
        xSize = grid.length;
        visitedPunishment = new double[ySize + 1][xSize + 1];
    }

    int xSize;
    int ySize;
    double visitedPunishmentBase = 1;
    double[][] visitedPunishment;

    double get(StateObservation obs) {
        Vector2d pos = obs.getAvatarPosition();
        int size = obs.getBlockSize();
        int x = (int) (pos.x / size);
        int y = (int) (pos.y / size);
        return get(x, y);
    }

    double get(int x, int y) {
        return visitedPunishment[y][x] / visitedPunishmentBase;
    }

    void punish(StateObservation obs) {
        Vector2d pos = obs.getAvatarPosition();
        int size = obs.getBlockSize();
        int x = (int) (pos.x / size);
        int y = (int) (pos.y / size);
        visitedPunishment[y][x] += visitedPunishmentBase;
        visitedPunishmentBase *= C.PUNISH_MULTIPLIER;
        if (visitedPunishmentBase > 1000) {
            for (int i = 0; i < ySize; ++i) {
                for (int j = 0; j < xSize; ++j) {
                    visitedPunishment[i][j] /= visitedPunishmentBase;
                }
            }
            visitedPunishmentBase = 1.0;
        }
    }
    
    void printPunishment() {
        for (int i = 0; i < ySize; ++i) {
            for (int j = 0; j < xSize; ++j) {
                double punish = get(j, i);
                if(punish != 0) punish = -Math.log(punish) / Math.log(2);
                String s = String.valueOf(Math.round(10 * punish));
                s = ("    " + s).substring(s.length());
                System.out.print(s);
            }
            System.out.println();
        }
        System.out.println();
    }
}
