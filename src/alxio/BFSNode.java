/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package alxio;

import core.game.StateObservation;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.PriorityQueue;
import ontology.Types;
import tools.ElapsedCpuTimer;

/**
 *
 * @author ALX
 */
public class BFSNode implements Comparable<BFSNode> {

    public static MersenneTwisterFast random = new MersenneTwisterFast();
    public static HashMap<Long, BFSNode> map;
    public static PriorityQueue<BFSNode> queue;
    public static BFSNode leader;
    public static CollisionAnalyzer analyser;
    public static Punisher punisher;

    static int currentEpoch = 0;
    public static int treeSize = 0;
    static BFSNode root;

    public static BFSNode init(StateObservation a_gameState) {
        map = new HashMap<>();
        queue = new PriorityQueue<>();
        punisher = new Punisher(a_gameState.getObservationGrid());
        analyser = new CollisionAnalyzer(a_gameState);
        treeSize = 0;
        return addToMap(null, a_gameState);
    }

    static BFSNode addToMap(BFSNode prev, StateObservation nextState) {
        long hash = Z.hash(nextState);
        BFSNode node = map.get(hash);

        if (node == null) {
            node = new BFSNode(nextState, hash, prev);
            queue.offer(node);
            map.put(hash, node);
            node.eval();
            node.updateLeader();
            ++treeSize;
        } else if (node.parent != null) {
            int oldDepth = node.parent.depth;
            int newDepth = prev == null ? Integer.MAX_VALUE : prev.depth;
            if (newDepth < oldDepth) {
                node.parent = prev;
            }
        }
        return node;
    }

    public static double evalState(StateObservation state) {
        int res = 0;
        for (Integer resource : state.getAvatarResources().values()) {
            res += resource;
        }
        boolean gameOver = state.isGameOver();
        Types.WINNER win = state.getGameWinner();
        double rawScore = state.getGameScore();

        if (gameOver && win == Types.WINNER.PLAYER_LOSES) {
            rawScore -= C.WIN_BONUS;
        }
        if (gameOver && win == Types.WINNER.PLAYER_WINS) {
            rawScore += C.WIN_BONUS;
        }
        return rawScore + C.RESOURCE_BONUS * res + analyser.eval(state) * C.HEURISTIC_BONUS;
    }

    /**
     * Generates random permutation of array [0, ..., size-1] Used to randomize
     * child visits
     *
     * @param size
     * @return
     */
    static int[] genArray(int size) {
        int[] ar = new int[size];
        for (int i = 0; i < size; ++i) {
            ar[i] = i;
        }
        for (int i = size - 1; i > 0; i--) {
            int index = random.nextInt(i + 1);
            // Simple swap
            int a = ar[index];
            ar[index] = ar[i];
            ar[i] = a;
        }
        return ar;
    }

    long zHash;
    ChildNodes[] children;
    StateObservation stateObs;
    BFSNode parent = null;

    double value = 0;
    double expectedValue = 0;

    int depth = 0;
    int epoch = 0;

    BFSNode(StateObservation state) {
        zHash = Z.hash(state);
        stateObs = state;
    }

    BFSNode(StateObservation state, long hash, BFSNode prnt) {
        zHash = hash;
        stateObs = state;
        parent = prnt;
        depth = prnt == null ? 0 : prnt.depth + 1;
    }

    @Override
    public String toString() {
        String s = super.toString();
        return s + " " + zHash + " " + stateObs.getAvatarPosition();
    }

    private void updateExpected() {
        if (children == null) {
            expectedValue = value;
        } else {
            double val = -Double.MAX_VALUE;
            double sum = 0;
            for (ChildNodes ch : children) {
                double exp = ch.expectedValue();
                if (exp > val) {
                    val = exp;
                }
            }
            //applies discount rate to gain, not to value
            expectedValue = C.DISCOUNT_RATE * val + (1 - C.DISCOUNT_RATE) * value;
        }
    }

    public int search(ElapsedCpuTimer elapsedTimer) {
        long remaining = elapsedTimer.remainingTimeMillis();
        int remainingLimit = 8;

//        double phaseOneLimit = 40;
//        
//        System.out.println(remaining + " " + phaseOneLimit);
//        Preserver preserver = new Preserver(stateObs);
//        while(remaining > phaseOneLimit && preserver.step()){
//            remaining = elapsedTimer.remainingTimeMillis();
//        }
        int grows = 0;
        int mcts = 0;
        int greed = 0;
        grow(this);
        while (!queue.isEmpty() && remaining > remainingLimit && treeSize < Strategus.MAX_TREE_SIZE) {
            if (random.nextDouble() < Strategus.getSimpleRolloutChance()) {
                simpleRollout();
            }
            if (random.nextDouble() < Strategus.ROLLOUT_CHANCE) {
                mcts(false);
            } else if (greed++ < C.GREEDY_COUNT) {
                greedyGrow();
            } else if (grows++ < C.QUEUE_GROW_COUNT) {
                growFromQueue();
            } else if (mcts++ < C.MCTS_COUNT) {
                mcts(true);
            } else {
                grows = mcts = 0;
            }
            remaining = elapsedTimer.remainingTimeMillis();
        }

        int selected = -1;
        BFSNode best = leader;//Strategus.DETERMINISTIC_CHOICE ? leader : null;

        double chance = 1.0;
        if (best != null) {
            for (int k = 0; k < 100; ++k) {
                if (best.parent == null) {
                    break;
                }
                if (chance < 0.5) {
                    break;
                }
                BFSNode next = best.parent;
                double maxPercent = 0;
                int id = -1;
                for (int i = 0; i < next.children.length; ++i) {
                    double percent = next.children[i].contains(best);
                    if (percent > maxPercent) {
                        id = i;
                        maxPercent = percent;
                    }
                }
                chance *= maxPercent;
                if (next.parent == this) {
                    selected = id;
                    break;
                } else {
                    best = next;
                }
            }
        }

        if (selected == -1) {
            selected = uct(-1, 10, false);
        }

        Debug.Log(2, stateObs.getAvatarPosition().mul(1.0 / stateObs.getBlockSize()).toString());
        Debug.Log(1, Game.ActionName(selected));
        if (Debug.level > 1) {
            for (int i = 0; i < children.length; ++i) {
                System.out.print(Game.ActionName(i) + " " + children[i].typesCount + " ");
                System.out.println(children[i].expectedValue() + " " + children[i].mctsTotalValue / (children[i].mctsTotalVisits + 1));
            }
        }
        return selected;
    }

    public BFSNode proceed(int lastAction, StateObservation obs) {
        long hash = Z.hash(obs);
        punisher.punish(obs);
        if (hash == zHash) {
            return this;
        }
        if (lastAction != -1) {
            BFSNode node = (BFSNode) children[lastAction].findByHashCode(hash);
            if (node != null) {
                return node;
            }
        }
        return addToMap(parent, obs);
    }

    public void clean() {
        Debug.printGrid(stateObs.getObservationGrid());
        leader = null;
        parent = null;
        queue.clear();
        ++currentEpoch;
        depth = 0;
        epoch = currentEpoch;
        treeSize = 0;
        int maxDepth = 0;

        ArrayDeque<BFSNode> Q = new ArrayDeque<>();
        ArrayDeque<BFSNode> Q1 = new ArrayDeque<>();

        Q.push(this);
        while (!Q.isEmpty()) {
            ++treeSize;
            BFSNode node = Q.remove();
            node.updateLeader();
            if (node.children == null) {
                queue.add(node);
            } else {
                for (ChildNodes list : node.children) {
                    list.mctsTotalValue *= C.DECAY_RATE;
                    list.mctsTotalVisits *= C.DECAY_RATE;
                    for (Pair<Double, BFSNode> child : list) {
                        if (child.y.epoch < currentEpoch) {
                            child.y.eval();
                            child.y.epoch = currentEpoch;
                            child.y.depth = node.depth + 1;
                            if (child.y.depth > maxDepth) {
                                maxDepth = child.y.depth;
                            }
                            Q.add(child.y);
                            Q1.addFirst(child.y);
                        }
                    }
                }
            }
        }
        for (BFSNode node : Q1) {
            node.updateExpected();
        }
        Q1.clear();
        ArrayList<Long> toDelete = new ArrayList<>();
        for (Map.Entry<Long, BFSNode> entry : map.entrySet()) {
            if (entry.getValue().epoch != currentEpoch) {
                toDelete.add(entry.getKey());
            }
        }
        for (Long id : toDelete) {
            map.remove(id);
        }
        Strategus.updateStrategy(treeSize);
    }

    private void growFromQueue() {
        BFSNode curr = queue.remove();
        while (!grow(curr) && !queue.isEmpty()) {
            curr = queue.remove();
        }
    }

    private boolean grow(BFSNode curr) {
        if (curr.children != null) {
            return false;
        }
        curr.children = new ChildNodes[Game.NUM_ACTIONS];
        int[] indices = genArray(curr.children.length);
        for (int i : indices) {
            StateObservation nextState = curr.stateObs.copy();
            nextState.advance(Game.actions[i]);
            Strategus.onAdvance(nextState);
            //TODO instead of hashing orientation can use this
//            if (nextState.getAvatarPosition().equals(curr.stateObs.getAvatarPosition())
//                    && !nextState.getAvatarOrientation().equals(curr.stateObs.getAvatarOrientation())) {
//                nextState.advance(Game.actions[i]);
//            }
            analyser.analyze(nextState, curr.stateObs.getGameScore());
            BFSNode next = addToMap(curr, nextState);
            curr.children[i] = new ChildNodes(next, curr);
        }
        return true;
    }

    private void eval() {
        expectedValue = value = evalState(stateObs) - depth * C.DEPTH_PENETLY - punisher.get(stateObs) * C.VISITED_PENETLY;
    }

    private void updateLeader() {
        if (leader == null || value > leader.value) {
            Debug.printGrid(stateObs.getObservationGrid());
            leader = this;
        }
    }

    @Override
    public int compareTo(BFSNode o) {
        if (value > o.value) {
            return -1;
        }
        if (value < o.value) {
            return 1;
        }
        if (depth < o.depth) {
            return -1;
        }
        if (depth > o.depth) {
            return 1;
        }
        return 0;
    }

    private void greedyGrow() {
        ArrayDeque<ChildNodes> toUpdate = new ArrayDeque<>();
        BFSNode selected = this;
        StateObservation tmpState = null;
        while (selected.children != null) {
            int childId = -1;
            double childValue = -Double.MAX_VALUE;

            for (int i = 0; i < children.length; ++i) {
                double exp = children[i].expectedValue();
                if (exp > childValue) {
                    childValue = exp;
                    childId = i;
                }
            }

            if (childId == -1) {
                break;
            }

            ChildNodes child = selected.children[childId];
            toUpdate.addFirst(child);

            BFSNode next;
            if (child.isSure()) {
                tmpState = null;
                next = child.get();
            } else {
                if (tmpState == null) {
                    tmpState = selected.stateObs.copy();
                }
                double val = tmpState.getGameScore();
                tmpState.advance(Game.actions[childId]);
                Strategus.onAdvance(tmpState);
                analyser.analyze(tmpState, val);
                next = child.addNode(tmpState, selected);
            }
            if (next.depth <= selected.depth) {
                break;
            }
            selected = next;
        }
        grow(selected);
        backPropagate(toUpdate, selected, true);
    }

    private void mcts(boolean grow) {
        ArrayDeque<ChildNodes> toUpdate = new ArrayDeque<>();
        BFSNode selected = this;
        StateObservation tmpState = null;
        while (selected.children != null) {
            int childId = selected.uct(C.K, 1, true);
            if (childId == -1) {
                break;
            }
            ChildNodes child = selected.children[childId];
            toUpdate.addFirst(child);
            if (child.isSure()) {
                tmpState = null;
                selected = child.get();
            } else {
                if (tmpState == null) {
                    tmpState = selected.stateObs.copy();
                }
                double val = tmpState.getGameScore();
                tmpState.advance(Game.actions[childId]);
                Strategus.onAdvance(tmpState);
                analyser.analyze(tmpState, val);
                selected = child.addNode(tmpState, selected);
            }
        }
        if (grow) {
            backPropagate(toUpdate, selected, grow(selected));
        } else {
            rollout(tmpState, toUpdate, selected);
        }
    }

    //toUpdate have leaf in begin and root in end
    private void backPropagate(ArrayDeque<ChildNodes> toUpdate, BFSNode selected, boolean withValue) {
        double updateValue = 0;
        int count = children.length;
        if (withValue) {
            double values[] = new double[children.length];
            for (int i = 0; i < children.length; ++i) {
                values[i] = selected.children[i].nodes.get(0).value;
            }
            Arrays.sort(values);
            int start = 0;
            for (int i = 0; i < children.length / 2; ++i) {
                if (values[i] < selected.value) {
                    ++start;
                }
            }
            for (int i = start; i < children.length; ++i) {
                updateValue += values[i];
            }
            count = children.length - start;
        }
        for (ChildNodes updated : toUpdate) {
            updated.mctsTotalValue += updateValue;
            updated.mctsTotalVisits += count;
            BFSNode prnt = updated.parent.get();
            if (prnt != null) {
                prnt.updateExpected();
            }
            updateValue *= C.DISCOUNT_RATE;
        }
    }

//    Pair<Integer, Double> sumUp(ChildNodes child) {
//        Pair<Integer, Double> answer = new Pair<>(0, 0.0);
//        for (Pair<Double, BFSNode> node : child) {
//            answer.y += node.x * (node.y.totalValue + DISCOUNT_RATE * node.y.value) / (node.y.nVisits + 1);
//            answer.x += node.y.nVisits;
//        }
//        return answer;
//    }
    private int uct(double K, double G, boolean onlyDown) {
        int selected = -1;
        double bestValue = -Double.MAX_VALUE;

        double allVisits = 0;
        for (ChildNodes child : children) {
            allVisits += child.mctsTotalVisits;
        }

        for (int i = 0; i < children.length; ++i) {
            ChildNodes child = children[i];
            //Disallow goint down the tree.
            if (onlyDown && child.mostProbableNode().depth <= depth) {
                continue;
            }
            double childVisits = child.mctsTotalVisits + 1;
            double childValue = child.mctsTotalValue / childVisits;
            double uctValue = childValue
                    + K * Math.sqrt(Math.log1p(allVisits) / childVisits)
                    + G * child.expectedValue();
            if (uctValue > bestValue) {
                selected = i;
                bestValue = uctValue;
            }
        }
        return selected;
    }

    private void rollout(StateObservation obs, ArrayDeque<ChildNodes> toUpdate, BFSNode selected) {
        double updateValue = 0;
        if (obs == null) {
            obs = selected.stateObs.copy();
        }
        int lastDirection = random.nextInt(Game.actions.length);
        for (int i = 0; i < C.ROLLOUT_LENGTH && !obs.isGameOver(); ++i) {
            double val = obs.getGameScore();
            if (random.nextBoolean()) {
                lastDirection = random.nextInt(Game.actions.length);
            }
            obs.advance(Game.actions[lastDirection]);
            Strategus.onAdvance(obs);
            analyser.analyze(obs, val);
        }
        updateValue = evalState(obs);

//        if (updateValue < 0 && selected.value > 0) {
//            updateValue = 0;
//        }
        for (ChildNodes updated : toUpdate) {
            updated.mctsTotalValue += updateValue;
            updated.mctsTotalVisits += 1;
            updateValue *= C.DISCOUNT_RATE;
        }
    }

    private void simpleRollout() {
        StateObservation so = this.stateObs.copy();
        int action = random.nextInt(Game.NUM_ACTIONS);
        so.advance(Game.actions[action]);
        Strategus.onAdvance(so);
        BFSNode node = children[action].addNode(so, this);
        ArrayDeque<ChildNodes> toUpdate = new ArrayDeque<>();
        toUpdate.add(this.children[action]);
        rollout(null, toUpdate, node);
    }
}
