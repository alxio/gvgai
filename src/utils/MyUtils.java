/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utils;

import core.game.Observation;
import java.util.ArrayList;

/**
 *
 * @author ALX
 */
public class MyUtils {
    public static void printGrid(ArrayList<Observation>[][] grid){
        if(1==1)return;
        for(int i=0;i<grid[0].length;++i){
            for(int j=0;j<grid.length; ++j){
                ArrayList<Observation> obs = grid[j][i];
                if(obs.size() == 0){
                    System.out.print("    ");
                }
                else if(obs.size() == 1){
                    String s = obs.get(0).itype + "";
                    s = ("    " + s).substring(s.length());
                    System.out.print(s);
                }else{
                    //TODO SLOW!!
                    String ss = "";
                    for(Observation o : obs){
                        ss += " ";
                        ss += o.itype;
                    }
                    ss = ("    " + ss).substring(ss.length());
                    System.out.print(ss);
                }
                //System.out.print(grid[i][j].size() + " ");
            }
            System.out.println();
        }
        System.out.println();
    }
}
