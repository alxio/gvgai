import core.ArcadeMachine;

import java.util.Random;

/**
 * Created with IntelliJ IDEA.
 * User: Diego
 * Date: 04/10/13
 * Time: 16:29
 * This is a Java port from Tom Schaul's VGDL - https://github.com/schaul/py-vgdl
 */
public class Test1
{

    public static void main(String[] args)
    {
        //Available controllers:
        String sampleRandomController = "controllers.sampleRandom.Agent";
        String sampleOneStepController = "controllers.sampleonesteplookahead.Agent";
        String sampleMCTSController = "controllers.sampleMCTS.Agent";
        String alxio = "alxio.Agent";
        String alxio2 = "alxio2.Agent";
        String sampleOLMCTSController = "controllers.sampleOLMCTS.Agent";
        String sampleGAController = "controllers.sampleGA.Agent";
        String tester = "controllers.Tester.Agent";
        
        String human = "controllers.human.Agent";

        //Available games:
        String gamesPath = "examples/gridphysics/";

        //CIG 2014 Training Set Games
        String games0[] = new String[]{"aliens", "boulderdash", "butterflies", "chase", "frogs",
                "missilecommand", "portals", "sokoban", "survivezombies", "zelda"};

        //CIG 2014 Validation Set Games
        String games1[] = new String[]{"camelRace", "digdug", "firestorms", "infection", "firecaster",
              "overload", "pacman", "seaquest", "whackamole", "eggomania"};

        //CIG 2015 New Training Set Games
        String games2[] = new String[]{"bait", "boloadventures", "brainman", "chipschallenge",  "modality",
                                      "painter", "realportals", "realsokoban", "thecitadel", "zenpuzzle"};
        
        String games3[] = new String[]{"roguelike", "surround", "catapults", "plants", "plaqueattack",
                "jaws", "labyrinth", "boulderchase", "escape", "lemmings"};
        
        String[] games = new String[40];
        for(int i=0;i<10;++i){
            games[i] = games0[i];
        }for(int i=0;i<10;++i){
            games[i+10] = games1[i];
        }for(int i=0;i<10;++i){
            games[i+20] = games2[i];
        }for(int i=0;i<10;++i){
            games[i+30] = games3[i];
        }
        
        //EXTRA GAMES:
        //String games[] = new String[]{ "solarfox", "bombuzal" };

        //Other settings
        boolean visuals = true;
        String recordActionsFile = null;//"Logs_" + System.currentTimeMillis(); //where to record the actions executed. null if not to save.
        int seed = new Random().nextInt();

        //Game and level to play
        int gameIdx = 31;
        int levelIdx = 0; //level names from 0 to 4 (game_lvlN.txt).
        String game = gamesPath + games[gameIdx] + ".txt";
        String level1 = gamesPath + games[gameIdx] + "_lvl" + levelIdx +".txt";

//        // 1. This starts a game, in a level, played by a human.
          //ArcadeMachine.playOneGame(game, level1, recordActionsFile, seed);
          //ArcadeMachine.playOneGame(game, level1, recordActionsFile, seed);
//        ArcadeMachine.playOneGame(game, level1, recordActionsFile, seed);
//        ArcadeMachine.playOneGame(game, level1, recordActionsFile, seed);
//        ArcadeMachine.playOneGame(game, level1, recordActionsFile, seed);

        // 2. This plays a game in a level by the controller.
        
        //ArcadeMachine.runOneGame(game, level1, visuals, tester, recordActionsFile, seed);
        //core.competition.CompetitionParameters.MAX_TIMESTEPS = 100;
        //ArcadeMachine.runOneGame(game, level1, visuals, sampleOLMCTSController, recordActionsFile, seed);
        //ArcadeMachine.runOneGame(game, level1, visuals, sampleMCTSController, recordActionsFile, seed);
        ArcadeMachine.runOneGame(game, level1, visuals, alxio, recordActionsFile, seed);
        //ArcadeMachine.runOneGame(game, level1, visuals, alxio, recordActionsFile, seed);
        //ArcadeMachine.runOneGame(game, level1, visuals, alxio, recordActionsFile, seed);
        //ArcadeMachine.runOneGame(game, level1, visuals, tester, recordActionsFile, seed);
        //ArcadeMachine.runOneGame(game, level1, visuals, sampleMCTSController, recordActionsFile, seed);
       
        // 3. This replays a game from an action file previously recorded
        //String readActionsFile = "actionsFile_aliens_lvl0.txt";  //This example is for
        //ArcadeMachine.replayGame(game, level1, visuals, readActionsFile);

//        // 4. This plays a single game, in N levels, M times :
//        String level2 = gamesPath + games[gameIdx] + "_lvl" + 1 +".txt";
//        int M = 5;
//        String[] con = {sampleMCTSControllerWithMemory, sampleMCTSController, tester};
//        for(String troller : con){
//            ArcadeMachine.runGames(game, new String[]{level1, level2}, M, troller, null); 
//        }

        //5. This plays N games, in the first L levels, M times each. Actions to file optional (set saveActions to true).
//        int N = 10, L = 1, M = 1;
//        boolean saveActions = false;
//        String[] levels = new String[L];
//        String[] actionFiles = new String[L*M];
////        //String[] trollers = {tester, sampleMCTSController, sampleOLMCTSController, sampleMCTSControllerWithMemory};
////        
////        //for(String con : trollers)
//        for(int i = 0; i < N; ++i)
//        {
//            int actionIdx = 0;
//            game = gamesPath + games[20+i] + ".txt";
//            for(int j = 0; j < L; ++j){
//                levels[j] = gamesPath + games[20+i] + "_lvl" + j +".txt";
//                if(saveActions) for(int k = 0; k < M; ++k)
//                    actionFiles[actionIdx++] = alxio + "actions_game_" + i + "_level_" + j + "_" + k + ".txt";
//            }
//            ArcadeMachine.runGames(game, levels, M, alxio, saveActions? actionFiles:null);
//        }
    }
}
