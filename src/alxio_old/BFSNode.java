/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package alxio_old;

import core.game.StateObservation;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.PriorityQueue;
import ontology.Types;
import tools.ElapsedCpuTimer;
import tools.Utils;

/**
 *
 * @author ALX
 */
public class BFSNode implements INode, Comparable<BFSNode> {
    public static final double epsilon = 1e-6;
    public static MersenneTwisterFast random = new MersenneTwisterFast();

    public static HashMap<Long, BFSNode> map;
    public static PriorityQueue<BFSNode> queue;
    public static BFSNode leader;

    public static int MAX_TREE_SIZE = 5000;
    
    static long mCopies = 0;
    static long mAdvances = 0;

    static BFSNode init(StateObservation a_gameState) {
        map = new HashMap<>();
        queue = new PriorityQueue<>();
        treeSize = 0;
        return addToMap(null, a_gameState);
    }

    long zHash;
    long zHash1;
    ChildNodes[] children;
    StateObservation stateObs;
    BFSNode parent = null;

    double value = 0;

    int depth = 0;
    int epoch = 0;

    static int currentEpoch = 0;

    public static int treeSize = 0;
    
    static BFSNode root;

    @Override
    public String toString(){
        String s = super.toString();
        return s + " " + zHash + " " + stateObs.getAvatarPosition();
    }
    
    BFSNode(StateObservation state) {
        zHash = Z.hash(state);
        zHash1 = Z1.hash(state);
        stateObs = state;
    }

    BFSNode(StateObservation state, long hash, long hash2, BFSNode prnt) {
        zHash = hash;
        zHash1 = hash2;
        stateObs = state;
        parent = prnt;
        depth  = prnt == null ? 0 : prnt.depth + 1;
    }

    public static int TICKS = 0;

    static int GROW_COUNT = 1;
    static int MCTS_COUNT = 0;
    
    int search(ElapsedCpuTimer elapsedTimer) {
        Debug.println("Search: " + this);
        ++TICKS;
        //double avgTimeTaken = 0;
        //double acumTimeTaken = 0;
        long remaining = elapsedTimer.remainingTimeMillis();
        int remainingLimit = 10;
        //int numIters = 0;

        int grows = 0;
        int mcts = 0;
        
        while (!queue.isEmpty() && remaining > remainingLimit && treeSize < MAX_TREE_SIZE) {
            if(grows++ < GROW_COUNT){
                treeGrowth();
            }else if(mcts < MCTS_COUNT){
                //mctsGrow();
            }else{
                grows = mcts = 0;
            }
            remaining = elapsedTimer.remainingTimeMillis();
        }
        
        int selected = -1;
        BFSNode best = leader;

        if (best == null) {
            Debug.log(1,"No best?!");
        } else if (best == this) {
            Debug.log(1,"Current state is best?!");
        } else {
            Debug.log(1,"Best is " + best);
            for (int k = 0;k < 100; ++k) {
                if (best.parent == null) {
                    Debug.println("Null parent");
                    break;
                }else if(best.parent == this){
                    Debug.println("Path ok, parent: " + this);
                    break;
                }
                Debug.println("Moving to " + best.parent);
                best = best.parent;
            }
            double maxPercent = 0;
            for (int i = 0; i < children.length; ++i) {
                double percent = children[i].contains(best);
                if (percent > maxPercent) {
                    selected = i;
                    maxPercent = percent;
                }
            }
        }

        if (selected == -1) {
            Debug.println("Path to leader not found");
        }

        Debug.log(1,"Copies: " + mCopies + " Advances: " + mAdvances);
        Debug.log(1,"Collisions " + (COLLISION) + "/" + OK + " NODES:" + NEW_NODE);
        return selected;
    }

    BFSNode proceed(int lastAction, StateObservation obs) {
        long hash = Z.hash(obs);
        if (hash == zHash) {
            return this;
        }
        Debug.log(1, "Observed state:" + hash);
        Debug.printGrid(obs.getObservationGrid());
        if (lastAction != -1) {
            BFSNode node = (BFSNode)children[lastAction].findByHashCode(hash);
            if (node != null) {
                return node;
            }
        }
        return addToMap(parent, obs);
    }

    void clean() {
        Runtime runtime = Runtime.getRuntime();
        if(runtime.totalMemory() - runtime.freeMemory() > 0.75 * runtime.maxMemory()){
            MAX_TREE_SIZE = (int) (0.9 * MAX_TREE_SIZE);
        }
        
        Debug.log(1, "Root state:" + Z.hash(stateObs));
        Debug.printGrid(stateObs.getObservationGrid());
        leader = null;
        parent = null;
        queue.clear();
        ++currentEpoch;
        depth = 0;
        epoch = currentEpoch;
        treeSize = 0;
        int maxDepth = 0;
        
        ArrayDeque<BFSNode> Q = new ArrayDeque<>();
        Q.push(this);
        while (!Q.isEmpty()) {
            ++treeSize;
            BFSNode node = Q.remove();
            node.updateLeader();
            Debug.println("Clean: " + node);
            if (node.children == null) {
                queue.add(node);
            } else {
                for (ChildNodes list : node.children) {
                    for (Pair<Double, BFSNode> child : list) {
                        if (child.y.epoch < currentEpoch) {
                            child.y.epoch = currentEpoch;
                            child.y.depth = node.depth + 1;
                            if(child.y.depth > maxDepth){
                                maxDepth = child.y.depth;
                            }
                            Q.add(child.y);
                        }
                        //TODO Maybe fuzzy parents
                    }
                }
            }
        }
        ArrayList<Long>toDelete = new ArrayList<>();
        for(Map.Entry<Long, BFSNode> entry : map.entrySet()){
            if(entry.getValue().epoch != currentEpoch){
                toDelete.add(entry.getKey());
            }
        }
        int DELETED = toDelete.size();
        for(Long id : toDelete){
            map.remove(id);
        }
        toDelete.clear();
        Debug.log(1, "SIZE: " + treeSize + " DEPTH: " + maxDepth + " DELETE: "+ DELETED + " TOTAL: " + Runtime.getRuntime().totalMemory() / 1048576);
        System.gc();
        Debug.log(1, "SIZE: " + treeSize + " DEPTH: " + maxDepth + " DELETE: "+ DELETED + " TOTAL: " + Runtime.getRuntime().totalMemory() / 1048576);
    }

    static long COLLISION = 0;
    static long OK = 0;
    static long NEW_NODE = 0;

    static BFSNode addToMap(BFSNode prev, StateObservation nextState) {
        mCopies++;
        mAdvances++;

        long hash = Z.hash(nextState);
        long hash2 = Z1.hash(nextState);
        BFSNode next = map.get(hash);

        if (next == null) {
            next = new BFSNode(nextState, hash, hash2, prev);
            queue.offer(next);
            Debug.println("Queue: " + next);
            map.put(hash, next);
            next.eval();
            next.updateLeader();
            ++treeSize;
            ++NEW_NODE;
        } else {
            if (hash2 != next.zHash1) {
                Debug.println("Collision! " + (++COLLISION) + "/" + OK + " NODES:" + NEW_NODE);
            } else {
                ++OK;
            }
        }
        return next;
    }

    private void treeGrowth() {
        BFSNode curr = queue.remove();
        Debug.println("Dequeue: " + curr);
        curr.children = new ChildNodes[Agent.NUM_ACTIONS];
        int[] indices = genArray(curr.children.length);
        for (int i : indices) {
            StateObservation nextState = curr.stateObs.copy();
            nextState.advance(Agent.actions[i]);
            BFSNode next = addToMap(curr, nextState);
            curr.children[i] = new ChildNodes(next);
        }
    }

    private void eval() {
        int res = 0;
        for (Integer resource : stateObs.getAvatarResources().values()) {
            res += resource;
        }
        boolean gameOver = stateObs.isGameOver();
        Types.WINNER win = stateObs.getGameWinner();
        double rawScore = stateObs.getGameScore();

        if (gameOver && win == Types.WINNER.PLAYER_LOSES) {
            rawScore -= 10000000;
        }

        if (gameOver && win == Types.WINNER.PLAYER_WINS) {
            rawScore += 10000000;
        }

        value = rawScore + 0.01 * res;
    }

    /**
     * Generates random permutation of array [0, ..., size-1]
     * Used to randomize child visits
     * @param size
     * @return 
     */
    static int[] genArray(int size) {
        int[] ar = new int[size];
        for (int i = 0; i < size; ++i) {
            ar[i] = i;
        }
        for (int i = size - 1; i > 0; i--) {
            int index = random.nextInt(i + 1);
            // Simple swap
            int a = ar[index];
            ar[index] = ar[i];
            ar[i] = a;
        }
        return ar;
    }

    @Override
    public long getHash() {
        return zHash;
    }

    private void updateLeader() {
        if (leader == null || value > leader.value) {
            Debug.log(1,"Leader " + this);
            Debug.printGrid(stateObs.getObservationGrid());
            leader = this;
        }
    }

    @Override
    public int compareTo(BFSNode o) {
        if(value > o.value) return -1;
        if(value < o.value) return 1;
        
        if(depth < o.depth) return -1;
        if(depth > o.depth) return 1;
        
        return 0;
    }

//    private void mctsGrow() {
//        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
//    }
//    
//        public BFSNode uct() {
//
//        BFSNode selected = null;
//        double bestValue = -Double.MAX_VALUE;
//        for (SingleTreeNode child : this.children)
//        {
//            double hvVal = child.totValue;
//            double childValue =  hvVal / (child.nVisits + this.epsilon);
//
//
//            childValue = Utils.normalise(childValue, bounds[0], bounds[1]);
//
//            double uctValue = childValue +
//                    Agent.K * Math.sqrt(Math.log(this.nVisits + 1) / (child.nVisits + this.epsilon));
//
//            // small sampleRandom numbers: break ties in unexpanded nodes
//            uctValue = Utils.noise(uctValue, this.epsilon, this.m_rnd.nextDouble());     //break ties randomly
//
//            // small sampleRandom numbers: break ties in unexpanded nodes
//            if (uctValue > bestValue) {
//                selected = child;
//                bestValue = uctValue;
//            }
//        }
//
//        if (selected == null)
//        {
//            throw new RuntimeException("Warning! returning null: " + bestValue + " : " + this.children.length);
//        }
//
//        return selected;
//    }
}
