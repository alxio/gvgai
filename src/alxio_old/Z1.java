package alxio_old;

import core.game.Observation;
import core.game.StateObservation;
import java.util.ArrayList;

/**
 *
 * @author ALX
 */
public class Z1 {
    public static long hash(StateObservation so){
        return instance.internalHash(so);
    }
    public static void init(int x, int y){
        instance = new Z1(new MersenneTwisterFast(), x, y);
    }
    
    private Z1(MersenneTwisterFast r, int x, int y){
        this.x = x;
        this.y = y;
        this.random = r;
    }
    private static Z1 instance = null;
    
    private long internalHash(StateObservation so){
        ArrayList<Observation>[][] grid = so.getObservationGrid();
        long hash = 0;
        if(grid.length != y || grid[0].length != x){
            throw new RuntimeException("Bad array size");
        }
        for(int i=0;i<y;++i){
            for(int j=0;j<x;++j){
                ArrayList<Observation>obs = grid[i][j];
                for(Observation o : obs){
                    initType(o.itype);
                    hash ^= values[o.itype][i][j];
                }
            }
        }
        return hash;
    }
    
    private void initType(int id){
        if(values[id] == null){
            values[id] = new long[y][x];
            for(int i=0;i<y;++i)for(int j=0;j<x;++j){
                values[id][i][j] = random.nextLong();
            }
        }
    }
    
    int y;
    int x;
    MersenneTwisterFast random;
    long[][][] values = new long[1000][][];
}
