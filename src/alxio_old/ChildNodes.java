/*
 * BFSNodeo change this license header, choose License Headers in Project Properties.
 * BFSNodeo change this template file, choose BFSNodeools | BFSNodeemplates
 * and open the template in the editor.
 */
package alxio_old;

import java.util.ArrayList;
import java.util.Iterator;

public class ChildNodes implements Iterable<Pair<Double, BFSNode>>{
    public ChildNodes(BFSNode node){
        addNode(node);
    }
    ArrayList<BFSNode> nodes = new ArrayList<>();
    ArrayList<Integer> count = new ArrayList<>();
    int typesCount = 0;
    int totalCount = 0;
    int a = 0, b = 0;
    
    BFSNode findByHashCode(long hash){
        for(int i=0;i<totalCount;++i){
            if(nodes.get(i).getHash() == hash){
                count.set(i, count.get(i) + 1);
                return nodes.get(i);
            }
        }
        return null;
    }
    
    static int sameCount = 0;
    static int otherCount = 0;
    
    final void addNode(BFSNode node){
        long h = node.getHash();
        for(int i=0;i<totalCount;++i){
            if(nodes.get(i).getHash() == h){
                count.set(i, count.get(i) + 1);
                ++sameCount;
            }
        }
        if(typesCount > 0){
            otherCount++;
        }
        nodes.add(node);
        count.add(1);
        typesCount++;
        totalCount++;
    }
    
    boolean isSure(){
        return (sameCount > 100 && otherCount == 0) || totalCount > 4 && typesCount == 1 || totalCount > 10;
    }
    
    double contains(BFSNode best) {
        double k = 0;
        long h = best.getHash();
        for(int i=0;i<typesCount;++i){
            if(nodes.get(i).getHash() == h){
                k = count.get(i);
                break;
            }
        }
        return k == 0 ? 0 : (k / totalCount); 
    }
    
    BFSNode get() {
        if(typesCount == 0) return null;
        if(b++ > count.get(a)){
            b = 0;
            a = (a+1) % typesCount;
        }
        return nodes.get(a);
    }

    @Override
    public Iterator<Pair<Double, BFSNode>> iterator() {
        return new ChildIterator();
    }
    
    class ChildIterator implements Iterator<Pair<Double, BFSNode>>{
        int next = 0;
        @Override
        public boolean hasNext() {
            return next < typesCount;
        }
        @Override
        public Pair<Double, BFSNode> next() {
            return new Pair<>(
                    ((double)count.get(next))/totalCount,
                    nodes.get(next++)
            );
        }
        @Override
        public void remove(){
            throw new UnsupportedOperationException();
        }
    }
}
