package alxio_old;

import core.game.StateObservation;
import java.util.HashMap;
import ontology.Types;
import tools.ElapsedCpuTimer;
import tools.Utils;

public class MCTSNode
{
    public static final double HUGE_NEGATIVE = -10000000.0;
    public static final double HUGE_POSITIVE =  10000000.0;
    public static final double epsilon = 1e-6;
    
    protected static double[] bounds = new double[]{Double.MAX_VALUE, -Double.MAX_VALUE};
    public static MersenneTwisterFast m_rnd = new MersenneTwisterFast();
    public static StateObservation rootState = null;
    
    public MCTSNode parent;
    public MCTSNode[] children;
    public double totValue;
    public int nVisits;
    public int m_depth;
    public long zHash;
    public long zHash1;
    
    public static HashMap<Long, MCTSNode> map;
    
    private int curr_depth;
    
//    @Override
//    public boolean equals(Object o){
//        if (this == o) {
//            return true;
//        }
//        if (o instanceof SingleTreeNode) {
//            return zHash == ((SingleTreeNode)o).zHash;
//        }
//        return false;
//    }
//
//    @Override
//    public int hashCode() {
//        return (int) (this.zHash ^ (this.zHash >>> 32));
//    }

    public MCTSNode(StateObservation state) {
        this(null, -1, state);
    }

    public MCTSNode(MCTSNode parent, int childIdx, StateObservation state) {
        this.parent = parent;
        zHash = Z.hash(state);
        zHash1 = Z1.hash(state);
        children = new MCTSNode[Agent.NUM_ACTIONS];
        totValue = 0.0;
        if(parent != null)
            m_depth = parent.m_depth+1;
        else
            m_depth = 0;
    }

    public void mctsSearch(ElapsedCpuTimer elapsedTimer) {
        curr_depth = m_depth;
        double avgTimeTaken = 0;
        double acumTimeTaken = 0;
        long remaining = elapsedTimer.remainingTimeMillis();
        int numIters = 0;
        int remainingLimit = 10;
        
        while(remaining > 2*avgTimeTaken && remaining > remainingLimit){
            StateObservation state = rootState.copy();

            ElapsedCpuTimer elapsedTimerIteration = new ElapsedCpuTimer();
            MCTSNode selected = treePolicy(state);
            double delta = selected.rollOut(state);
            backUp(selected, delta);

            numIters++;
            acumTimeTaken += (elapsedTimerIteration.elapsedMillis()) ;
            //Debug.println(elapsedTimerIteration.elapsedMillis() + " --> " + acumTimeTaken + " (" + remaining + ")");
            avgTimeTaken  = acumTimeTaken/numIters;
            remaining = elapsedTimer.remainingTimeMillis();
        }
    }

    public MCTSNode treePolicy(StateObservation state) {

        MCTSNode cur = this;
        
        int i = 0;
        
        while (!state.isGameOver() && ++i < Agent.ROLLOUT_DEPTH)
        {
            if (cur.notFullyExpanded()) {
                return cur.expand(state);
            } else {
                MCTSNode next = cur.uct(state);
                cur = next;
            }
        }

        return cur;
    }


    public MCTSNode expand(StateObservation state) {

        int bestAction = 0;
        double bestValue = -1;

        for (int i = 0; i < children.length; i++) {
            double x = m_rnd.nextDouble();
            if (x > bestValue && children[i] == null) {
                bestAction = i;
                bestValue = x;
            }
        }

        //Roll the state
        state.advance(Agent.actions[bestAction]);
        
        long hash = Z.hash(state);
        long hash1 = Z1.hash(state);
        MCTSNode tn = map.get(hash);
        
        if(tn == null){
            tn = new MCTSNode(this, bestAction, state);
            map.put(hash, tn);
        }else {
            if (tn.zHash1 != hash1) {
                Debug.println("Collision detected.");
            }
            if(tn.parent != null && tn.parent.m_depth > m_depth){
                backUp(tn.parent, -tn.totValue, -tn.nVisits);
                backUp(this, tn.totValue, tn.nVisits);
                tn.parent = this;
            }
        }
        
        children[bestAction] = tn;
        return tn;
    }

    public MCTSNode uct(StateObservation state) {

        MCTSNode selected = null;
        double bestValue = -Double.MAX_VALUE;
        int childIdx = -1;
        
        for(int i=0;i<children.length;++i){
            MCTSNode child = children[i];
            double hvVal = child.totValue;
            double childValue =  hvVal / (child.nVisits + this.epsilon);

            childValue = Utils.normalise(childValue, bounds[0], bounds[1]);
            //Debug.println("norm child value: " + childValue);

            double uctValue = childValue +
                    Agent.K * Math.sqrt(Math.log(this.nVisits + 1) / (child.nVisits + this.epsilon));
            
            int depthFactor = child.m_depth - m_depth;
            
            uctValue = depthFactor * Utils.noise(uctValue, this.epsilon, this.m_rnd.nextDouble());     //break ties randomly

            // small sampleRandom numbers: break ties in unexpanded nodes
            if (uctValue > bestValue) {
                childIdx = i;
                selected = child;
                bestValue = uctValue;
            }
        }
        if (selected == null)
        {
            throw new RuntimeException("Warning! returning null: " + bestValue + " : " + this.children.length + " " +
            + bounds[0] + " " + bounds[1]);
        }

        //Roll the state:
        state.advance(Agent.actions[childIdx]);
        return selected;
    }


    public double rollOut(StateObservation state)
    {
        int depth = this.m_depth - this.curr_depth;

        while (!finishRollout(state,depth)) {
            int action = m_rnd.nextInt(Agent.NUM_ACTIONS);
            state.advance(Agent.actions[action]);
            depth++;
        }

        double delta = value(state);

        if(delta < bounds[0])
            bounds[0] = delta;
        if(delta > bounds[1])
            bounds[1] = delta;

        //double normDelta = Utils.normalise(delta ,lastBounds[0], lastBounds[1]);

        return delta;
    }

    public double value(StateObservation a_gameState) {

        boolean gameOver = a_gameState.isGameOver();
        Types.WINNER win = a_gameState.getGameWinner();
        double rawScore = a_gameState.getGameScore();

        if(gameOver && win == Types.WINNER.PLAYER_LOSES)
            rawScore += HUGE_NEGATIVE;

        if(gameOver && win == Types.WINNER.PLAYER_WINS)
            rawScore += HUGE_POSITIVE;

        return rawScore;
    }

    public boolean finishRollout(StateObservation rollerState, int depth)
    {
        if(depth >= Agent.ROLLOUT_DEPTH)      //rollout end condition.
            return true;

        if(rollerState.isGameOver())               //end of game
            return true;

        return false;
    }

    public void backUp(MCTSNode node, double result){
        backUp(node, result, 1);
    }
    
    public void backUp(MCTSNode node, double result, int visits)
    {
        MCTSNode n = node;
        while(n != null)
        {
            n.nVisits += visits;
            n.totValue += result;
            n = n.parent;
        }
    }


    public int mostVisitedAction() {
        int selected = -1;
        double bestValue = -Double.MAX_VALUE;
        boolean allEqual = true;
        double first = -1;

        for (int i=0; i<children.length; i++) {

            if(children[i] != null)
            {
                if(first == -1)
                    first = children[i].nVisits;
                else if(first != children[i].nVisits)
                {
                    allEqual = false;
                }

                double childValue = children[i].nVisits;
                childValue = Utils.noise(childValue, this.epsilon, this.m_rnd.nextDouble());     //break ties randomly
                if (childValue > bestValue) {
                    bestValue = childValue;
                    selected = i;
                }
            }
        }

        if (selected == -1)
        {
            Debug.println("Unexpected selection!");
            selected = 0;
        }else if(allEqual)
        {
            //If all are equal, we opt to choose for the one with the best Q.
            selected = bestAction();
        }
        return selected;
    }

    public int bestAction()
    {
        int selected = -1;
        double bestValue = -Double.MAX_VALUE;

        for (int i=0; i<children.length; i++) {

            if(children[i] != null) {
                //double tieBreaker = m_rnd.nextDouble() * epsilon;
                double childValue = children[i].totValue / (children[i].nVisits + this.epsilon);
                childValue = Utils.noise(childValue, this.epsilon, this.m_rnd.nextDouble());     //break ties randomly
                if (childValue > bestValue) {
                    bestValue = childValue;
                    selected = i;
                }
            }
        }

        if (selected == -1)
        {
            Debug.println("Unexpected selection!");
            selected = 0;
        }

        return selected;
    }


    public boolean notFullyExpanded() {
        for (MCTSNode tn : children) {
            if (tn == null) {
                return true;
            }
        }

        return false;
    }
}
