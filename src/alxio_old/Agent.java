package alxio_old;

import core.game.Observation;
import core.game.StateObservation;
import core.player.AbstractPlayer;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.TreeSet;
import ontology.Types;
import tools.ElapsedCpuTimer;

/**
 * Created with IntelliJ IDEA. User: ssamot Date: 14/11/13 Time: 21:45 This is a
 * Java port from Tom Schaul's VGDL - https://github.com/schaul/py-vgdl
 */
public class Agent extends AbstractPlayer {

    public static int NUM_ACTIONS;
    public static int ROLLOUT_DEPTH = 10;
    public static double K = Math.sqrt(2);
    public static Types.ACTIONS[] actions;

    public BFSNode m_root;

    public void init(StateObservation a_gameState) {
        if(m_root != null){ 
            m_root = m_root.proceed(lastAction, a_gameState);
            m_root.clean();
        }
        if (m_root == null) {
            m_root = BFSNode.init(a_gameState);
        }
    }

    /**
     * Public constructor with state observation and time due.
     *
     * @param so state observation of the current game.
     * @param elapsedTimer Timer for the controller creation.
     */
    public Agent(StateObservation so, ElapsedCpuTimer elapsedTimer) {
        
        ArrayList<Observation>[][] grid = so.getObservationGrid();

        Z.init(grid[0].length, grid.length);
        Z1.init(grid[0].length, grid.length);

        ArrayList<Types.ACTIONS> act = so.getAvailableActions();
        actions = new Types.ACTIONS[act.size()];
        for (int i = 0; i < actions.length; ++i) {
            actions[i] = act.get(i);
        }
        NUM_ACTIONS = actions.length;
//      m_root = new BFSNode(so);
//      BFSNode.map = new HashMap<>();
//      BFSNode.queue = new ArrayDeque<>();
//      BFSNode.queue.add(m_root);
//      m_root.search(elapsedTimer);
    }

    private int lastAction = -1;

    /**
     * Picks an action. This function is called every game step to request an
     * action from the player.
     *
     * @param stateObs Observation of the current state.
     * @param elapsedTimer Timer when the action returned is due.
     * @return An action for the current state
     */

    static String dummy = null;
    
    @Override
    public Types.ACTIONS act(StateObservation stateObs, ElapsedCpuTimer elapsedTimer) {
//        try {
            init(stateObs);
            lastAction = m_root.search(elapsedTimer);
            if (lastAction == -1) {
                Debug.println("Oops, returning null action.");
                return Types.ACTIONS.ACTION_NIL;
            }
            //Debug.println("Action: " + actions[lastAction]);
            return actions[lastAction];
//        } catch (Throwable e) {
//            throw OOMError.create(BFSNode.treeSize, BFSNode.TICKS, e);
//        }
    }
}
